export interface IConfig {
  mqtt: {
    host: string;
    username: string;
    password: string;
    id: string;
    path: string;
  };
  melcloud: {
    username: string;
    password: string;
    interval: number;
  };
}

const DEFAULT_INTERVAL_MS = '5000'; // milliseconds

export const config: IConfig = {
  mqtt: {
    host: process.env.MQTT_HOST || '',
    username: process.env.MQTT_USERNAME || '',
    password: process.env.MQTT_PASSWORD || '',
    id: process.env.MQTT_ID || '',
    path: process.env.MQTT_PATH || 'melcloud',
  },

  melcloud: {
    username: process.env.MELCLOUD_USERNAME || '',
    password: process.env.MELCLOUD_PASSWORD || '',
    interval: parseInt(
      process.env.MELCLOUD_INTERVAL || DEFAULT_INTERVAL_MS,
      10,
    ),
  },
};
