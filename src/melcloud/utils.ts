export const bodyParamsToFormPost = (params: {
  [key: string]: string | number | boolean | undefined;
}): URLSearchParams => {
  const formParams = new URLSearchParams();
  Object.entries(params).forEach(([key, value]) => {
    if (value !== undefined) {
      formParams.append(key, value.toString());
    }
  });
  return formParams;
};

const format = (type: string, args: string[]) =>
  [`[${type.toUpperCase()}]`, ...args].join(' ');

export const log = (type: string, ...args: string[]) =>
  console.log(format(type, args)); // eslint-disable-line no-console

export const error = (type: string, ...args: string[]) =>
  console.error(format(type, args)); // eslint-disable-line no-console

export const test = 1;
