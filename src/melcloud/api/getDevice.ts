export interface IWeatherObservation {
  Date: string;
  Sunrise: string;
  Sunset: string;
  Condition: number;
  ID: number;
  Humidity: number;
  Temperature: number;
  Icon: string;
  ConditionName: string;
  Day: number;
  WeatherType: number;
}

export interface IGetDeviceResponse {
  EffectiveFlags: number;
  LocalIPAddress: string | null;
  RoomTemperature: number;
  SetTemperature: number;
  SetFanSpeed: number;
  OperationMode: number;
  VaneHorizontal: number;
  VaneVertical: number;
  Name: string | null;
  NumberOfFanSpeeds: number;
  WeatherObservations?: IWeatherObservation[];
  ErrorMessage: string | null;
  ErrorCode: number;
  DefaultHeatingSetTemperature: number;
  DefaultCoolingSetTemperature: number;
  HideVaneControls: boolean;
  HideDryModeControl: boolean;
  RoomTemperatureLabel: number;
  InStandbyMode: boolean;
  TemperatureIncrementOverride: number;
  ProhibitSetTemperature: boolean;
  ProhibitOperationMode: boolean;
  ProhibitPower: boolean;
  DeviceID: number;
  DeviceType: number;
  LastCommunication: string;
  NextCommunication: string;
  Power: boolean;
  HasPendingCommand: boolean;
  Offline: boolean;
  Scene: string | null;
  SceneOwner: string | null;
}
